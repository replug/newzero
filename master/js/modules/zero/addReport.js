App.controller('AddReportController', ['$scope', function($scope) {
  'use strict';

// Datepicker
  // ----------------------------------- 

  $scope.report = {
    dt: null
  }

  $scope.today = function() {
    $scope.dt = new Date();
    //INit date to today
    $scope.report.dt = new Date();
  };
  $scope.today();

  $scope.clear = function () {
    $scope.dt = null;
  };

  // Disable weekend selection
  $scope.disabled = function(date, mode) {
    return false; //( mode === 'day' && ( date.getDay() === 0 || date.getDay() === 6 ) );
  };

  $scope.toggleMin = function() {
    $scope.minDate = $scope.minDate ? null : new Date();
  };
  $scope.toggleMin();

  $scope.open = function($event) {
    $event.preventDefault();
    $event.stopPropagation();

    $scope.opened = true;
  };

  $scope.dateOptions = {
    formatYear: 'yy',
    startingDay: 1
  };

  $scope.initDate = new Date('2016-15-20');
  $scope.formats = ['dd/MM/yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
  $scope.format = $scope.formats[0];


  /////////////////////////////////

//Add new report 

  $scope.addR = function(report){

    $scope.reports = [];

  	Report.save(report).then(function (report) {
      $scope.$apply(function () {
          $scope.reports.push(report);
      });
  });
    console.log(report);
  };

//View Recent reports

Report.readAll().then(function (reports) {
  $scope.$apply(function () {
      $scope.reports = reports;
  });
});

}]);